<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('dashboard');
});

Auth::routes();

Route::get('dashboard', 'BonusController@dashboard');

Route::get('bonuses/assign-user/{id?}', 'BonusController@assignUserForm');

Route::post('bonuses/assign-user', 'BonusController@assignUserPost');

Route::get('/bonuses/assign-user-list/{id?}', 'BonusController@assignUserList');

Route::resource('bonuses', 'BonusController');

