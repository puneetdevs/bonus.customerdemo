@extends('layouts.app')

@section('style')
<style>
.container {
  padding: 2rem 0rem;
}

h4 {
  margin: 2rem 0rem 1rem;
}

.table-image {
  td, th {
    vertical-align: middle;
  }
}

.dashboard_heading{
  color: blue;
}

.numberCircle {
  display:inline-block;
  line-height:0px;
  

  border-radius:50%;
  border:5px solid yellowgreen;

  font-size:32px;
  
  color: blue;
}

.numberCircle span {
  display:inline-block;

  padding-top:50%;
  padding-bottom:50%;

  margin-left:100px;
  margin-right:100px;
}

.add_bonus{
  margin-bottom:50px;
}



</style>
@endsection

@section('content')
<div class="container-fluid">
  <div class="h1_box">
    <h2>Dashboard</h2> <a class="button btn btn-primary float-right" href="{{url('bonuses/create')}}">Add Bonus</a>

  </div>

  <div class="box_cus">
    <div class="row">
      <div class="col-sm-4">
        <div class="box_cus_row">
          <h3>Total Bonuses Added</h3>
          <h6><a href="{{url('bonuses')}}"><?php echo ($bonuses) && !empty($bonuses) ? count($bonuses) : 0 ; ?></a></h6>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="box_cus_row">
          <h3>Total Bonuses Assigned To Users</h3>
          <h6><a href="{{url('bonuses/assign-user-list')}}"><?php echo ($bonusUser) && !empty($bonusUser) ? count($bonusUser) : 0 ; ?></a></h6> 
        </div>
      </div>
    </div>
  </div>
  
@endsection