@extends('layouts.app')

@section('style')
<style>
.container {
  padding: 3rem 0rem;
}
</style>
@endsection

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        @include('breadcrumb.default', ['current_page' => 'Assign Bonus','main_page' => 'Bonus Listing', 'main_page_url' => 'bonuses'])
        <div class="col-sm-12">
            <div class="flash-message" id="successMessage">
                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                    @if(Session::has('alert-' . $msg))
                    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}</p>
                    @endif
                @endforeach
            </div>
            <div class="card">
                <div class="card-header">Assign Bonus</div>
                <div class="card-body">
                <form method="POST" action="{{url('bonuses/assign-user')}}">
                    <div class="row">
                    
                    <div class="col-sm-6">
                        @csrf
                    
                    <div class="form-group">
                        <label for="amount">Bonuses</label>
                        <select class="custom-select" name="bonus_id">
                            <option value="" selected>Select Bonus</option>
                            @foreach($bonuses as $bonus)
                                <option {{ ($id) && $id == $bonus->id ? 'selected' : ''}} value="{{$bonus->id}}">{{$bonus->name}}</option>
                            @endforeach
                        </select>
                        @if($errors->has('bonus_id'))
                            <div class="alert-danger">{{ $errors->first('bonus_id') }}</div>
                        @endif
                    </div>
                </div>
<div class="col-sm-6">
                    <div class="form-group">
                        <label for="amount">Users</label>
                        <select class="custom-select" name="user_id">
                            <option value="" selected>Select User</option>
                            @foreach($users as $user)
                                <option value="{{ $user->id }}">{{ $user->name }}</option>
                            @endforeach
                        </select>
                        @if($errors->has('user_id'))
                            <div class="alert-danger">{{ $errors->first('user_id') }}</div>
                        @endif
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="amount">Amount (&euro;)</label>
                        <input type="number" name="amount" value="{{ old('amount') }}" class="form-control" placeholder="Amount">
                        @if($errors->has('amount'))
                            <div class="alert-danger">{{ $errors->first('amount') }}</div>
                        @endif
                    </div>
                </div><div class="col-sm-12">

                    <button type="submit" class="btn btn-primary">Submit</button></div></div>   
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('#start_date').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd'
    });

    $('#end_date').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd'
    });
    $(document).ready(function(){
        setTimeout(function() {
            $('#successMessage').hide();
        }, 2000);
    });
</script>
@endsection