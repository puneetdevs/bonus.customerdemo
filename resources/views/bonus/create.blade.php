@extends('layouts.app')

@section('style')
<style>
.container {
  padding: 3rem 0rem;
}
</style>
@endsection

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
    @include('breadcrumb.default', ['current_page' => 'Add Bonus','main_page' => 'Bonus Listing', 'main_page_url' => 'bonuses'])
        <div class="col-md-12 mb-4">
            <div class="card">
                <div class="card-header">Add Bonus</div>

                <div class="card-body">
                <form method="POST" action="{{url('bonuses')}}">
                    @csrf
                    <div class="row">
                        <div class="col-sm-6">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" class="form-control" placeholder="Name" value="{{ old('name') }}">
                        @if($errors->has('name'))
                            <div class="alert-danger">{{ $errors->first('name') }}</div>
                        @endif
                    </div>
                    </div>
 <div class="col-sm-6">
                    <div class="form-group">
                        <label for="name_invoice">Invoice Name</label>
                        <input type="text" name="name_invoice" class="form-control" placeholder="Invoice Name" value="{{ old('name_invoice') }}">
                        @if($errors->has('name_invoice'))
                            <div class="alert-danger">{{ $errors->first('name_invoice') }}</div>
                        @endif
                    </div>
                </div>
 <div class="col-sm-6">
                    <div class="form-group">
                        <label for="date_start">Start Date</label>
                        <input type="text" name="date_start" class="form-control" id="start_date" value="{{ old('date_start') }}">
                        @if($errors->has('date_start'))
                            <div class="alert-danger">{{ $errors->first('date_start') }}</div>
                        @endif
                    </div>
                </div>
 <div class="col-sm-6">
                    <div class="form-group">
                        <label for="date_end">End Date</label>
                        <input type="text" name="date_end" class="form-control" id="end_date" value="{{ old('date_end') }}">
                        @if($errors->has('date_end'))
                            <div class="alert-danger">{{ $errors->first('date_end') }}</div>
                        @endif
                    </div>
                </div>
 <div class="col-sm-6">
                    <div class="form-group">
                        <label for="amount">Amount (&euro;)</label>
                        <input type="number" name="amount" class="form-control" placeholder="Amount" value="{{ old('amount') }}">
                        @if($errors->has('amount'))
                            <div class="alert-danger">{{ $errors->first('amount') }}</div>
                        @endif
                    </div>
                </div>
                 <div class="col-sm-12">

                    <div class="form-group">
                        <label for="message">Message</label>
                        <textarea class="form-control" name="message" rows="3"></textarea>
                    </div>
                </div>
                 <div class="col-sm-12">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('#start_date').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd'
    });

    $('#end_date').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd'
    });
</script>
@endsection