@extends('layouts.app')

@section('style')
<style>
.container {
  padding: 2rem 0rem;
}

h4 {
  margin: 2rem 0rem 1rem;
}

.table-image {
  td, th {
    vertical-align: middle;
  }
}

.add_bonus{
  float:right;
  margin-top:25px;
}
</style>
@endsection

@section('content')
<div class="listing_page">
<div class="container-fluid">
  
    @include('breadcrumb.default', ['current_page' => 'Bonus Listing','main_page' => 'Dashboard', 'main_page_url' => 'dashboard'])
        <div class="h1_box">
          <h2>Bonus Listing</h2>
          <a class="button btn btn-primary float-right" href="{{url('bonuses/create')}}">Add Bonus</a>
        </div>
    
    <div class="col-12">
      <div class="flash-message" id="successMessage">
            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
              @if(Session::has('alert-' . $msg))
              <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}</p>
              @endif
          @endforeach
      </div>
    </div>
    <div class="col-12">
      <div class="table-responsive">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th scope="col">Bonus Name</th>
            <th scope="col">Users</th>
            <th scope="col">Invoice Name</th>
            <th scope="col">Amount (&euro;)</th>
            <th scope="col">Start Date</th>
            <th scope="col">End Date</th>
            <th scope="col">Entry Date</th>
            <th scope="col">Message</th>
            <th scope="col">Actions</th>
          </tr>
        </thead>
        <tbody>
            @foreach($bonuses as $bonus)
          <tr>
            <td>{{ $bonus->name }}</td>
            <td>
              @if(count($bonus->bonus_user) > '0')
                <a href="{{url('bonuses/assign-user-list/'.$bonus->id.'')}}">{{ count($bonus->bonus_user) }}</a>
              @else
                0
              @endif              
            </td>
            <td>{{ $bonus->name_invoice }}</td>
            <td>{{ $bonus->amount }}</td>
            <td>{{ $bonus->date_start }}</td>
            <td>{{ $bonus->date_end }}</td>
            <td>{{ date('Y-m-d',strtotime($bonus->date_entry)) }}</td>
            <td>{{ $bonus->message }}</td>
            <td class="nowrap">
                <a type="button" href="{{url('bonuses/'.$bonus->id.'/edit')}}" class="btn btn-primary">Edit</a>
                <a type="button" href="{{url('bonuses/assign-user/'.$bonus->id.'')}}" class="btn btn-primary">Assign</a>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
      </div>
      <div class="page_navi">
      @include('pagination.default', ['paginator' => $bonuses])
      </div>
    </div>
  </div>
</div>
 
<script>
$(document).ready(function(){
  setTimeout(function() {
    $('#successMessage').hide();
  }, 2000);
});
</script>
@endsection