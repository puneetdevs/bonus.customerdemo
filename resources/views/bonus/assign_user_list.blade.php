@extends('layouts.app')

@section('style')
<style>
.container {
  padding: 2rem 0rem;
}

h4 {
  margin: 2rem 0rem 1rem;
}

.table-image {
  td, th {
    vertical-align: middle;
  }
}

.bonus_filter{
  float:right;
  margin-top:25px;
}

.col-sm-12.breadcrumbs {
    padding: 0;}

</style>
@endsection


@section('content')
<div class="listing_page">
<div class="container-fluid">
 
    @include('breadcrumb.default', ['current_page' => 'Bonus Users','main_page' => 'Bonus Listing', 'main_page_url' => 'bonuses'])
 <div class="row">
    <div class="col-sm-8"><h4>Bonus Users Listing</h4> </div>
    <div class="col-sm-4">
      <select class="custom-select bonus_filter button btn btn-primary" id="bonus_id">
          <option value="" selected>Filter By Bonus Name</option>
          <option ($bonus_id) && $bonus_id == 'all' ? 'selected' : ''}} value="all">All</option>
          @foreach($bonuses as $bonus)
              <option {{ ($bonus_id) && $bonus->bonus_id == $bonus_id ? 'selected' : ''}} value="{{$bonus->id}}">{{$bonus->name}}</option>
          @endforeach
      </select>
    </div>
      
    <div class="col-12">
      <div class="table-responsive">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th scope="col">Bonus Name</th>
            <th scope="col">User Name</th>
            <th scope="col">Amount (&euro;)</th>
            <th scope="col">Entry Date</th>
          </tr>
        </thead>
        <tbody>
            @foreach($bonusUsers as $bonusUser)
          <tr>
            <td>{{$bonusUser->bonus->name}}</td>
            <td>{{$bonusUser->user->name}}</td>
            <td>{{$bonusUser->amount}}</td>
            <td>{{date('Y-m-d',strtotime($bonusUser->date_entry))}}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
      </div>
      <div class="page_navi">
      @include('pagination.default', ['paginator' => $bonusUsers])
      </div>
    </div>
  </div>
</div>
</div>
<script>
  $('select').on('change', function(){
    var selected_id =  $(this).val();
    window.location.href = "<?php echo url('bonuses/assign-user-list') ?>/"+selected_id;
  });
</script>
@endsection
