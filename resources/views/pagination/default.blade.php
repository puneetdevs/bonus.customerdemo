<style>
    .pagination_li {
        margin-left: 18px;
        border: 1px solid;
        padding: 7px;
    }
</style>

<b>Total : <?php echo $paginator->total();  ?></b>
<ul class="pagination">
    @if($paginator->currentPage() != 1)
    <li class="pagination_li {{ ($paginator->currentPage() == 1) ? ' disabled' : '' }}">
        <a href="{{ $paginator->url(1) }}">Previous</a>
    </li>
    @endif

    @for ($i = 1; $i <= $paginator->lastPage(); $i++)
        <li class="pagination_li {{ ($paginator->currentPage() == $i) ? ' active' : '' }}">
            <a href="{{ $paginator->url($i) }}">{{ $i }}</a>
        </li>
    @endfor

    @if($paginator->currentPage() != $paginator->lastPage())
    <li class="pagination_li">
        <a href="{{ $paginator->url($paginator->currentPage()+1) }}" >Next</a>
    </li>
    @endif
</ul>