<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bonus extends Model
{
    protected $table = 'bonus_main';

    protected $fillable = [
        'name',
        'name_invoice',
        'date_entry',
        'date_start',
        'date_end',
        'amount',
        'message'
    ];

    public $timestamps = false;

    //bonus_main relation with bonus_user
    public function bonus_user(){
        return $this->hasMany(BonusUser::class,'bonus_id','id');
    }
}
