<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BonusUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'required|max:250|unique:bonus_main,name,'.$this->id.',id',
            'name_invoice'  => 'required|max:250',
            'amount'        => 'required|numeric',
            'date_start'    => 'required|date|before:date_end',
            'date_end'      => 'required|date|after:date_start',
        ];
    }
    
}
