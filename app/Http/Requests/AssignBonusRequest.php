<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AssignBonusRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'bonus_id' => 'required|exists:bonus_main,id',
            'user_id'  => 'required|exists:users,id',
            'amount'   => 'required|numeric'
        ];
    }

    public function messages()
    {

        return [
            'bonus_id.required' => 'Bonus is required',
            'user_id.required'  => 'User is required',
        ];
    }
    
}
