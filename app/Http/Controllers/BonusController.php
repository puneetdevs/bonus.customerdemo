<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bonus;
use App\BonusUser;
use App\User;
use App\Http\Requests\BonusStoreRequest;
use App\Http\Requests\BonusUpdateRequest;
use App\Http\Requests\AssignBonusRequest;
use Session;

class BonusController extends Controller
{
    /**
     * Display a dashbord value.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {
        $bonuses = Bonus::get();
        $bonusUser = BonusUser::get();
        return view('bonus.dashboard')->with(['bonuses' => $bonuses, 'bonusUser' => $bonusUser]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bonuses = Bonus::with('bonus_user')->orderBy('id','desc')->paginate(10);

        return view('bonus.list')->with('bonuses', $bonuses);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('bonus.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\BonusStoreRequest;  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BonusStoreRequest $request)
    {
        $bonus = Bonus::create($request->input());
        Session::flash('alert-success', 'Bonus has been added successfully.');
        return redirect('/bonuses')->with('success', 'Bonus saved!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bonus = Bonus::find($id);

        return view('bonus.edit')->with('bonus', $bonus);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BonusUpdateRequest $request, $id)
    {
        $bonus = Bonus::find($id);

        $bonus->update($request->input());
        Session::flash('alert-success', 'Bonus has been updated successfully.');
        return redirect('/bonuses')->with('success', 'Bonus updated!');
    }

    /**
     * Show Assign user to bonus form data
     *
     * @return \Illuminate\Http\Response
     */
    public function assignUserForm($id = null)
    {
        $bonuses = Bonus::get();
        $users = User::get();
        
        return view('bonus.assign_user')
        ->with(['bonuses' => $bonuses,
                'users' => $users,
                'id' => $id]);
    }

    /**
     * Save Assign user to bonus
     *
     * @return \Illuminate\Http\Response
     */
    public function assignUserPost(AssignBonusRequest $request)
    {
        if($this->checkAleadyExist($request->input())){
            BonusUser::create($request->input());
            Session::flash('alert-success', 'The Bonus successfully assigned to user!'); 
            return redirect('/bonuses');
        }else{
            Session::flash('alert-danger', 'This bonus already assigned to selected user.'); 
            return back();
        }
    }

    /**
     * Check same bonus assign to same User
     *
     * return True/False
     */
    private function checkAleadyExist($request){
        $result = BonusUser::where('bonus_id',$request['bonus_id'])->where('user_id',$request['user_id'])->first();
        return ($result) ? false : true;
    }

    /**
     * Get list of all Assign user to bonus
     *
     * @return \Illuminate\Http\Response
     */
    public function assignUserList($id = null)
    {
        if($id && $id != 'all'){
            $bonusUsers = BonusUser::where('bonus_id',$id)->orderBy('date_entry','desc')->paginate(10);
        }else{
            $bonusUsers = BonusUser::orderBy('date_entry','desc')->paginate(10);
        }
        $bonuses = Bonus::get();
        return view('bonus.assign_user_list')
            ->with(['bonusUsers' => $bonusUsers, 
                    'bonuses' => $bonuses,
                    'bonus_id' => $id]);
    }
}
